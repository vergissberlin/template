tt_content.template_listgroup = COA
tt_content.template_listgroup {
    10 =< lib.stdheader
    20 = FLUIDTEMPLATE
    20 {
        file = {$plugin.template_contentelements.view.templateRootPath}Bootstrap/ListGroup.html
        partialRootPath = {$plugin.template_contentelements.view.partialRootPath}
        layoutRootPath = {$plugin.template_contentelements.view.layoutRootPath}
    }
}
tt_content.template_accordion = COA
tt_content.template_accordion  {
    10 =< lib.stdheader
    20 = FLUIDTEMPLATE
    20 {
        file = {$plugin.template_contentelements.view.templateRootPath}Bootstrap/Accordion.html
        partialRootPath = {$plugin.template_contentelements.view.partialRootPath}
        layoutRootPath = {$plugin.template_contentelements.view.layoutRootPath}
    }
}
tt_content.template_carousel = COA
tt_content.template_carousel {
    10 =< lib.stdheader
    20 = FLUIDTEMPLATE
    20 {
        file = {$plugin.template_contentelements.view.templateRootPath}Bootstrap/Carousel.html
        partialRootPath = {$plugin.template_contentelements.view.partialRootPath}
        layoutRootPath = {$plugin.template_contentelements.view.layoutRootPath}
    }
}
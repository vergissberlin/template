mod.wizards {
    newContentElement {
        wizardItems {
            bootstrap {
                header = LLL:EXT:template/Resources/Private/Language/Backend.xlf:theme_name
                elements {
                    template_carousel {
                        icon = gfx/c_wiz/regular_header.gif
                        title = LLL:EXT:template/Resources/Private/Language/Backend.xlf:content_element.carousel
                        description = LLL:EXT:template/Resources/Private/Language/Backend.xlf:content_element.carousel.description
                        tt_content_defValues {
                            CType = template_carousel
                        }
                    }
                    template_accordion {
                        icon = gfx/c_wiz/regular_header.gif
                        title = LLL:EXT:template/Resources/Private/Language/Backend.xlf:content_element.accordion
                        description = LLL:EXT:template/Resources/Private/Language/Backend.xlf:content_element.accordion.description
                        tt_content_defValues {
                            CType = template_accordion
                        }
                    }
                    template_panel {
                        icon = gfx/c_wiz/regular_header.gif
                        title = LLL:EXT:template/Resources/Private/Language/Backend.xlf:content_element.panel
                        description = LLL:EXT:template/Resources/Private/Language/Backend.xlf:content_element.panel.description
                        tt_content_defValues {
                            CType = template_panel
                        }
                    }
                    template_listgroup {
                        icon = gfx/c_wiz/regular_header.gif
                        title = LLL:EXT:template/Resources/Private/Language/Backend.xlf:content_element.listgroup
                        description = LLL:EXT:template/Resources/Private/Language/Backend.xlf:content_element.listgroup.description
                        tt_content_defValues {
                            CType = template_listgroup
                        }
                    }
                }
                show = *
            }
        }
    }
}